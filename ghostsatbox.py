#!/usr/bin/env python
#
# hostname='127.0.0.1', port=7356
#
#commands:
#
#https://github.com/csete/gqrx/blob/master/resources/remote-control.txt 
#
# f - Get frequency [Hz]
# F - Set frequency [Hz]
# m - Get demodulator mode
# M - <mode> [passband] Set demodulator mode (OFF, RAW, AM, FM, WFM, WFM_ST,
#  WFM_ST_OIRT, LSB, USB, CW, CWL, CWU)
# l STRENGTH - Get signal strength [dBFS]
# l SQL - Get squelch threshold [dBFS]
# L SQL <sql> - Set squelch threshold to <sql> [dBFS]
# u RECORD - Get status of audio recorder
# U RECORD <status> - Set status of audio recorder to <status>
# c - Close connection
# AOS - Acquisition of signal (AOS) event, start audio recording
# LOS - Loss of signal (LOS) event, stop audio recording

import os.path
import sys
import argparse
import telnetlib
import time

HOST = '127.0.0.1'
PORT = 7356
tn = telnetlib.Telnet(HOST, PORT)

freqPresets = ['250450000', '255350000', '261450000', '244185000', '265550000']

savedfreq = "00000"
savedmode = "null" 
savedfilter = "00000"

parser = argparse.ArgumentParser(description='Change the parameters of the satellite ghostbox with the following arguments:')
parser.add_argument('-s', '--skiptime', type=float, metavar='', default=1, help='change skiptime')
parser.add_argument('-m', '--mode', type=str, metavar='', default='FM', help='change mode')
parser.add_argument('-f', '--filter', type=int, metavar='', default=6000, help='change filter')
args = parser.parse_args()

skiptime = args.skiptime

def requestActive():
	tn.write(('?\n'.encode('ascii')))
	receivedMessage=tn.read_some().decode('ascii')
	print(receivedMessage)
	
#get freq, mode and filter size and store
def requestFreq():
	#get current freqency
	tn.write(('f\n'.encode('ascii'))) 
	savedfreq = tn.read_some().decode('ascii')
	#get current mode
	tn.write(('m\n'.encode('ascii')))
	savedmode = tn.read_some().decode('ascii')
	print('current frequency and mode:\n', savedfreq, savedmode)

def changeMode(mode, filter):
	tn.write(("M {x} {y}\n".format(x=mode, y=filter).encode('ascii')))
  	
def changeFreq(freq):
	tn.write(("F {x}\n".format(x=freq).encode('ascii'))) #format string
	print('frequency changed to: {x}'.format(x=freq))
	time.sleep(skiptime)
 
requestActive()
requestFreq()
changeMode(args.mode, args.filter)

while True:
	for freqPreset in freqPresets:
		changeFreq(freqPreset)


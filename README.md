# Ghost satellite box

### Usage:

* Activate remote control in GQRX 

* `python ghostsatbox.py -s 0.5 -m FM -f 6000`

### Arguments:

*  `-s, --skiptime` - change skiptime between frequencies 
* `-m, --mode` - change mode (FM, AM, USB, LSB, RAW, CW, OFF...)
* `-f, --filter` - change filter (Hz)
